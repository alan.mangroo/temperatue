package com.mangroo.temperature.integration;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.mangroo.temperature.TemperatureConfiguration;
import com.mangroo.temperature.data.Temperature;
import com.mangroo.temperature.data.repo.TemperatureRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TemperatureConfiguration.class)
//@ActiveProfiles("INTEGRATION_TEST")
public class TemperatureRepositoryIT {

    Logger logger = LoggerFactory.getLogger(TemperatureRepositoryIT.class);

    @Autowired
    private TemperatureRepository temperatureRepository;

    @Before
    public void before() {
        logger.info("Deleting all temperature records before test");
        temperatureRepository.deleteAll();
    }

    @After
    public void after() {
        logger.info("Deleting all temperature records after test");
        temperatureRepository.deleteAll();
    }

    @Test
    public void findsByName() {
        Temperature temperature = Temperature.builder()
                .name("Bedroom")
                .temperatureReading(25.6f)
                .timestamp(new Date())
                .build();
        temperatureRepository.save(temperature);
        List<Temperature> foundTemperature = temperatureRepository.findByName("Bedroom");
        assertThat(foundTemperature).hasSize(1);
        assertThat(foundTemperature.get(0).getName()).isEqualTo("Bedroom");
        assertThat(foundTemperature.get(0).getTemperatureReading()).isEqualTo(25.6f);
    }

}
